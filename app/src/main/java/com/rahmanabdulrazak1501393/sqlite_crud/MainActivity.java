package com.rahmanabdulrazak1501393.sqlite_crud;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final int MY_PERMISSIONS_REQUEST = 99;//int bebas, maks 1 byte
    GoogleApiClient mGoogleApiClient ;
    Location mLastLocation;


    private TextView etName;
    private TextView etAddress;
    private Button btnInsert;
    private Button btnUpdate;
    private ListView lvProfile;
    private List<String> mListProfile = new ArrayList<>();
    private Button btnDelete;
    DatabaseHandler handler = new DatabaseHandler(this);
    ProfileModel profile = new ProfileModel();

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void ambilLokasi() {
    /* mulai Android 6 (API 23), pemberian persmission
       dilakukan secara dinamik (tdk diawal)
       untuk jenis2 persmisson tertentu, termasuk lokasi
    */

        // cek apakah sudah diijinkan oleh user, jika belum tampilkan dialog
        // pastikan permission yg diminta cocok dgn manifest
        if (ActivityCompat.checkSelfPermission (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED )
        {
            //belum ada ijin, tampilkan dialog
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST);
            return;
        }
        //ambil lokasi terakhir
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        //isi lokasi ke user interface
        if (mLastLocation != null) {
            etName.setText("Latitude:"+String.valueOf(mLastLocation.getLatitude()));
            etAddress.setText("Longitude:"+String.valueOf(mLastLocation.getLongitude()));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        etName = (TextView) findViewById(R.id.edit_text_name); //Latitude
        etAddress = (TextView) findViewById(R.id.edit_text_address); //Longitude
        btnInsert = (Button) findViewById(R.id.button_insert);
        lvProfile = (ListView) findViewById(R.id.listview_contact);
        btnDelete = (Button) findViewById(R.id.button_delete);
        btnUpdate = (Button) findViewById(R.id.button_update);



        //insert
        // lakukan validasi terlebih dahulu, jika sudah benar, maka lakukan proses insert data
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(etName.getText().toString())){
                    etName.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else if(TextUtils.isEmpty(etAddress.getText().toString())){
                    etAddress.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else{
                    insertData(handler);
                }
            }
        });

        //Membaca semua data profile
        displayAllData(handler, profile);

        //menunculkan popup delete data profile
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupDelete();
            }
        });

        //memunculkan popup update data profile
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDataFromPopup();
            }
        });




    }


    private void displayAllData(DatabaseHandler handler, ProfileModel profile){
        mListProfile.clear();
        List<ProfileModel> list = handler.getAllDataProfile();
        for(ProfileModel profileModel : list){
            String log = "ID : " + profileModel.getId() + "\n" +
                    "Lat : " + profileModel.getName() + "\n" +
                    "Lng : " + profileModel.getAddress();
            System.out.println(log);

            //add to list
            mListProfile.add(log);

            //set to the model
            profile.setId(profileModel.getId());
            profile.setName(profileModel.getName());
            profile.setAddress(profileModel.getAddress());
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mListProfile);
        lvProfile.setAdapter(arrayAdapter);
        lvProfile.invalidateViews();
        arrayAdapter.notifyDataSetChanged();
    }


    private void insertData(final DatabaseHandler databaseHandler){
        String name = etName.getText().toString();
        String address = etAddress.getText().toString();

        ProfileModel profileModel = new ProfileModel();
        profileModel.setName(name);
        profileModel.setAddress(address);

        databaseHandler.addProfile(profileModel);
        displayAllData(databaseHandler, profileModel);

        etAddress.setText("");
        etName.setText("");
    }


    /**
     * Method ini digunakan untuk menampilkan popup delete dan melakukan proses delete data
     */
    private void popupDelete(){
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.lbl_choose_an_id);
        dialog.setContentView(R.layout.popup_delete);

        final SharedPreferences sharedPreferences = getSharedPreferences("mPrefs", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        final Spinner spinner= (Spinner) dialog.findViewById(R.id.spinner_id);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);

        final List<ProfileModel> profileModelList = handler.getAllDataProfile();

        //set data ke dalam spinner
        ArrayAdapter<ProfileModel> spinnerArrayAdapter = new ArrayAdapter<ProfileModel>(this,android.R.layout.simple_spinner_item, profileModelList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        //proses onclick pada item spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                editor.putString("IDprofile", profileModelList.get(i).getId()+"");
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String profileId = sharedPreferences.getString("IDprofile", null);
                handler.deleteRow(profileId);
                displayAllData(handler,profile);
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    /**
     * Method ini digunakan untuk menampilkan popup update data dan melakukan proses update data
     */
    private void updateDataFromPopup(){
        final Dialog dialog  = new Dialog(this);
        dialog.setTitle(R.string.lbl_update_data_pop_up);
        dialog.setContentView(R.layout.popup_update);

        Spinner spinnerUpdate = (Spinner) dialog.findViewById(R.id.spinner_update);
        final TextView etUpdateName = (TextView) dialog.findViewById(R.id.et_update_name);
        final TextView etUpdatePhone = (TextView) dialog.findViewById(R.id.et_update_address);
        Button btnSaveUpdate = (Button) dialog.findViewById(R.id.btn_save_update);

        final List<ProfileModel> profileUpdate = handler.getAllDataProfile();
        final SharedPreferences sharedPreferences = getSharedPreferences("mPrefs", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();


        //set data ke dalam spinner
        ArrayAdapter<ProfileModel> spinnerArrayAdapter = new ArrayAdapter<ProfileModel>(this,android.R.layout.simple_spinner_item, profileUpdate);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUpdate.setAdapter(spinnerArrayAdapter);

        //proses onclick pada item spinner
        spinnerUpdate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                etUpdateName.setText(profileUpdate.get(i).getName());
                etUpdatePhone.setText(profileUpdate.get(i).getAddress());
                editor.putString("id-for-update", profileUpdate.get(i).getId()+"");
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //saat button save ditekan lakukan validasi terlebih dahulu terhadapt field2 nya, jika sudah
        // benar semua maka lakukan update
        btnSaveUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(etUpdateName.getText().toString())){
                    etUpdateName.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else if(TextUtils.isEmpty(etUpdatePhone.getText().toString())){
                    etUpdatePhone.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else{
                    String newName = etUpdateName.getText().toString();
                    String newPhone = etUpdatePhone.getText().toString();
                    String idProfile = sharedPreferences.getString("id-for-update",null);

                    //update data
                    handler.updatedetails(Integer.parseInt(idProfile), newName, newPhone);
                    displayAllData(handler,profile);
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        ambilLokasi();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

}